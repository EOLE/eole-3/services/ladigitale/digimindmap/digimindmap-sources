<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digimindmap'][$id]['reponse'])) {
		$reponse = $_SESSION['digimindmap'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digimindmap_cartes WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($carte = $stmt->fetchAll()) {
			$admin = false;
			if (count($carte, COUNT_NORMAL) > 0 && $carte[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $carte[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digimindmap'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digimindmap'][$id]['digidrive'];
			}
			echo json_encode(array('nom' => $carte[0]['nom'], 'donnees' => $donnees, 'admin' =>  $admin, 'digidrive' => $digidrive));
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
